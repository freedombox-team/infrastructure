"use strict";

// SPDX-License-Identifier: AGPL-3.0-or-later

var all_status_url = "/status/pipeline-info.json";

var generatePipelineDiv = function (groupName, data) {
    var outerDiv = $('<div></div>');
    var ulList = $('<ul></ul>');
    var title = $('<h2>' + groupName + '</h2>');

    data.forEach(function(d) {
        var style = d.result.toLowerCase();
        var suffix = groupName.replace('freedombox-', '');
        var format = d.name == 'functional-tests' ? 'html' : 'log';

        ulList.append($(
            '<li>' +
                '<div class="build-name id="' + d.name + '-name">' +
                '<a href=' + `"https://ftp.freedombox.org/pub/freedombox/logs/${d.name}-${suffix}.${format}"` + '>' +
                    d.name + '</a>' +
                '</div> ' +
                '<div class="build-status ' + style + '" id="' + d.name + '-status">' +
                    d.result +
                '</div>' +
                '<div class="build-time id="' + d.name + '-time">' +
                    d.time +
                '</div>' +
            '</li>'));
    });

    outerDiv.append(title);
    outerDiv.append(ulList);

    return outerDiv;
};

$.get(all_status_url, function(response) {
    response.forEach(function(group) {
        Promise.all(group.pipelines.map(function(pipeline) {
            return pipeline;
        }).map(function(pipelineName) {
            let groupName = group.name.split("-").pop();
            return $.get("/status/" + pipelineName + "-" + groupName + "-status.json").catch(
                function(error) {
                    return {
                        "pipeline_name": pipelineName,
                        "result": "Unknown"
                    };
                });
        })).then(function(values) {
            return values.map(function(value) {
                let time_string = value.time === undefined ? "&nbsp;" : (new Date(value.time * 1000)).toLocaleString();
                let takeTill = value.pipeline_name.lastIndexOf("-");
                let displayName = value.pipeline_name.substring(0, takeTill);
                return {
                    name: displayName,
                    result: value.result,
                    time: time_string
                };
            });
        }).then(function(statuses) {
            $("#builds").append(generatePipelineDiv(group.name, statuses));
        });
    });
}).fail(function (response) {
    $('#builds').append('<div class="error">Error fetching pipeline information.</div>');
});
