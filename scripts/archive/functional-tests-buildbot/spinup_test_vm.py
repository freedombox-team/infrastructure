#!/usr/bin/python3
# SPDX-License-Identifier: AGPL-3.0-or-later

import subprocess
import time

script = '''
set -xe

VBoxManage createvm --name "app-server" --register
VBoxManage modifyvm app-server --cpus 4
VBoxManage modifyvm app-server --memory 2048
VBoxManage modifyvm app-server --ostype Debian_64
VBoxManage modifyvm app-server --vram 16
VBoxManage storagectl app-server --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach app-server --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium freedombox-unstable_dailyupstream_all-amd64.vdi

# Networking
VBoxManage modifyvm app-server --nic1 nat --nictype1 82545EM --natpf1 'HTTPS,tcp,,4430,,443' --natpf2 'SSH,tcp,,3222,,22' --natpf3 'Samba,tcp,,4450,,445'

# Start server
( nohup VBoxHeadless --startvm app-server >/dev/null 2>/dev/null & )
'''

subprocess.run(['bash'], input=script.encode(), check=True)

success = False
for _ in range(30):
    try:
        subprocess.run([
            'curl', '--silent', '-k', '--fail', 'https://localhost:4430/plinth'
        ], check=True)
        success = True
        break
    except subprocess.CalledProcessError as error:
        print('Failed to connect.', error)
        pass

    time.sleep(10)

if not success:
    print('Failed to bring up Plinth in VM.')
    raise SystemExit(-1)
