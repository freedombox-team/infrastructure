#!/usr/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later

import re

from pathlib import Path

FREEDOMBOX_PACKAGE_REGEX = 'freedombox_[0-9.]+_all.deb'
DOCUMENTATION_PACKAGE_REGEX = 'freedombox-doc-[a-z]{2}_[0-9.]+_all.deb'

build_dir_path = Path(__file__).resolve().parent
deb_files_dir_path = Path(__file__).resolve().parent.parent

# Remove version numbers and move Debian packages to build directory
for fil in deb_files_dir_path.iterdir():
    if fil.is_file():
        if re.findall(FREEDOMBOX_PACKAGE_REGEX, fil.name) or re.findall(
                DOCUMENTATION_PACKAGE_REGEX, fil.name):
            fil.rename(build_dir_path /
                       (fil.name.partition('_')[0] + '_dev.deb'))
