# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import steps

from .. import common, conf
from ..common.builders import BuilderConfigFactory

TARGET_NAME = 'a20-olinuxino-lime2'
ARTIFACT_NAME = 'freedombox-stable_pioneer_a20-olinuxino-lime2-armhf.img.xz'


class PioneerBuilderConfigFactory(BuilderConfigFactory):
    """Factory to produce a builder configuration for
    FreedomBox Pioneer edition."""

    def builder_name(self):
        return self.target and self.target + '-pioneer'

    def get_artifact_rsync_url(self):
        """This image goes into the /pioneer directory on the server."""
        return common.rsync_url(conf.conf['upload']['hardware_path'].format(
            'pioneer', 'nightly'))

    def _get_web_seed_url(self):
        return (conf.conf['download']['hardware_url'].format('pioneer') +
                'nightly' + '/' + self.artifact_name)

    def build_steps(self):
        """Customized build steps for FreedomBox Pioneer edition."""
        build_mirror = conf.conf['common']['build_mirror']
        uboot_binary_name = 'u-boot-sunxi-with-spl.bin'
        uboot_url = (
            'https://ftp.freedombox.org/pub/freedombox/hardware/pioneer/'
            f'u-boot/{uboot_binary_name}')
        uncompressed_artifact_name = ARTIFACT_NAME.replace('.xz', '')
        return [
            steps.ShellCommand(name='download custom u-boot',
                               command=['wget', '-q',
                                        uboot_url], haltOnFailure=True),
            steps.ShellCommand(
                name='build FreedomBox image', command=[
                    'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
                    f'--build-mirror={build_mirror}',
                    f'--build-stamp={self.build_stamp}',
                    f'--distribution={self.distribution}', '--build-in-ram',
                    '--enable-backports', '--skip-compression', self.target
                ], haltOnFailure=True, timeout=3600),
            steps.ShellCommand(
                name='replace u-boot binary in image', command=[
                    'sudo', 'dd', 'bs=1k', 'seek=8', 'conv=notrunc',
                    f'if={uboot_binary_name}',
                    f'of=build/{uncompressed_artifact_name}'
                ], haltOnFailure=True),
            steps.ShellCommand(
                name='compress image', command=[
                    'sudo', 'xz', '-9', '--threads=0',
                    'build/' + uncompressed_artifact_name
                ], haltOnFailure=True, timeout=3600)
        ]


builders = [
    PioneerBuilderConfigFactory(TARGET_NAME, ARTIFACT_NAME,
                                distribution='stable',
                                build_stamp='pioneer').create()
]
