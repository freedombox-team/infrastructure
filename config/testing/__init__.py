# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import schedulers

from .builders import builders

# A single nightly scheduler for all the builders. Runs at 1 am on Saturdays.
# It is assumed that a release made on Tuesday will have migrated to testing by
# Saturday.
weekly_schedulers = [
    schedulers.Nightly(name='weekly-testing', branch='master',
                       builderNames=[builder.name for builder in builders],
                       hour=1, minute=0, dayOfWeek=5)  # Monday is 0
]

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

schedulers = weekly_schedulers + force_schedulers
