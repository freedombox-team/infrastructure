# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Unify all the configuration for buildbot here to be included in master.cfg.
"""

from buildbot.plugins import reporters

from . import conf  # noisort
from . import common, dev, pioneer, stable, testing, unstable
from .common.reporters import FreedomBoxCIStatusPush, LogUploader

change_sources = common.change_sources
schedulers = (dev.schedulers + pioneer.schedulers + stable.schedulers +
              testing.schedulers + unstable.schedulers)
builders = (dev.builders + pioneer.builders + stable.builders +
            testing.builders + unstable.builders)

_irc_conf = conf.conf['reporters']['irc']
_irc = reporters.IRC(
    _irc_conf['server'], _irc_conf['nick'], useColors=False, useSSL=True,
    port=_irc_conf['port'], channels=[{
        "channel": _irc_conf['channel']
    }], authz={('force', 'stop'): _irc_conf['users']},
    notify_events=['failure', 'exception', 'problem', 'recovery', 'worker'])

services = [
    _irc,
    FreedomBoxCIStatusPush(conf.conf['reporters']['http']['url']),
    LogUploader(conf.conf['reporters']['log']['url'])
]
