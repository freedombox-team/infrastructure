# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import steps

from .. import conf
from ..common.builders import BuilderConfigFactory

_DISTRIBUTIONS = ['testing', 'unstable']
_ARCHITECTURES = ['amd64', 'arm64']
_ARTIFACT_NAME = 'freedombox-{distribution}_dev_all-{architecture}.img.xz'

# Dev images are larger in size
_image_size = conf.conf['images']['dev']['size']


class DevBuilderConfigFactory(BuilderConfigFactory):
    """Factory to produce a builder configuration for development images."""

    def builder_name(self):
        return '-'.join([self.target, self.distribution, 'dev'])

    def build_steps(self):
        build_mirror = conf.conf['common']['build_mirror']
        return [
            steps.ShellCommand(
                name='build FreedomBox image', command=[
                    'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
                    f'--build-mirror={build_mirror}',
                    f'--build-stamp={self.build_stamp}',
                    f'--image-size={self.image_size}',
                    f'--distribution={self.distribution}', '--build-in-ram',
                    '--with-build-dep', self.target
                ], haltOnFailure=True, timeout=3600)
        ]


builders = [
    DevBuilderConfigFactory(
        architecture,
        _ARTIFACT_NAME.format(distribution=distribution,
                              architecture=architecture), _image_size,
        distribution=distribution, build_stamp='dev').create()
    for distribution in _DISTRIBUTIONS for architecture in _ARCHITECTURES
]


class VirtualBoxDevBuilderConfigFactory(BuilderConfigFactory):
    """Factory to create builder configuration for development builds."""

    def steps(self):
        build_mirror = conf.conf['common']['build_mirror']
        repo_url = conf.conf['repos']['freedom_maker']['url']
        return [
            steps.ShellCommand(name='cleanup build directory',
                               command=['sudo', 'rm', '-rf',
                                        'build'], haltOnFailure=True),
            steps.Git(name="checkout source code", repourl=repo_url,
                      mode='full', alwaysUseLatest=True, haltOnFailure=True),
            steps.FileDownload(name="download freedombox_dev.deb",
                               mastersrc="artifacts/freedombox_dev.deb",
                               workerdest="freedombox_dev.deb"),
            steps.FileDownload(name="download freedombox-doc-en_dev.deb",
                               mastersrc="artifacts/freedombox-doc-en_dev.deb",
                               workerdest="freedombox-doc-en_dev.deb"),
            steps.FileDownload(name="download freedombox-doc-es_dev.deb",
                               mastersrc="artifacts/freedombox-doc-es_dev.deb",
                               workerdest="freedombox-doc-es_dev.deb"),
            steps.ShellCommand(
                name='build FreedomBox image', command=[
                    'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
                    f'--build-mirror={build_mirror}',
                    '--custom-package=freedombox_dev.deb',
                    '--custom-package=freedombox-doc-en_dev.deb',
                    '--custom-package=freedombox-doc-es_dev.deb',
                    '--skip-compression', f'--build-stamp={self.build_stamp}',
                    '--build-in-ram', self.target
                ], haltOnFailure=True, timeout=3600),
            steps.ShellCommand(
                name='change ownership',
                command=['sudo', 'chown', '-R', 'buildbot:buildbot',
                         'build'], alwaysRun=True),
            steps.ShellCommand(name="change permissions",
                               command="chmod -R 644 build/*"),
            steps.FileUpload(name='upload the artifact to build main',
                             workersrc='build/' + self.artifact_name,
                             masterdest='artifacts/' + self.artifact_name),
        ]


builders.append(
    VirtualBoxDevBuilderConfigFactory(
        'virtualbox-amd64', 'freedombox-unstable_dailyupstream_all-amd64.vdi',
        distribution='dev', build_stamp='dailyupstream').create())
