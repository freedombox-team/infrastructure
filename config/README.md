# BuildBot Configuration

[BuildBot](https://buildbot.net) is a continuous integration framework that can be used to create custom configurations for CI/CD.

## Master

The entire `config` directory must be deployed to the BuildBot master.
Mordor is the hostname of the BuildBot server. This machine runs the BuildBot master.

## Workers

The workers are named after *Lord of the Rings* characters.

### Gimli
This worker is responsible for spinning up the vm called `app-server` which hosts the development version of Plinth to run functional tests on.

### Legolas
This worker builds all of the FreedomBox disk images for all the architectures using the tool `freedom-maker`.

### Bilbo
Builds Plinth deb package continuously on every push to the Plinth git repository.
Bilbo is the client that runs functional tests on the `app-server`. It uses pytest, geckodriver and firefox for this purpose. See the README file in the `functional_tests` directory of FreedomBox for more information on how functional tests work.

# Artifacts
The build artifacts will be published to https://ftp.freedombox.org/pub/freedombox
Some temporary artifacts might only be published to the BuildBot server.

## Sub-directories
/testing-latest - testing builds
/nightly - unstable and dev builds
/logs - all log files
