# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import schedulers

from .builders import builders

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

schedulers = force_schedulers
