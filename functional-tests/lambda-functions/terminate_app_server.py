# SPDX-License-Identifier: AGPL-3.0-or-later

import json

import boto3

ec2 = boto3.client('ec2')


# TODO This function should terminate app-servers only, not other instances
def lambda_handler(event, context):
    instance_id = event['instance_id']
    ec2.terminate_instances(InstanceIds=[instance_id])
    print(f'Terminated instance {instance_id} successfully.')
