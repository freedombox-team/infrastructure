#!/bin/bash
# SPDX-License-Identifier: AGPL-3.0-or-later

# The same cloud-init script is being used in the LaunchTemplates of all 3 Debian releases

PROJECT_ID=$(curl -s http://169.254.169.254/latest/meta-data/tags/instance/salsa:project-id)
BUILD_JOB_ID=$(curl -s http://169.254.169.254/latest/meta-data/tags/instance/salsa:build-job-id)

wget -q -O artifacts.zip https://salsa.debian.org/api/v4/projects/$PROJECT_ID/jobs/$BUILD_JOB_ID/artifacts

apt-get update

apt-get install unzip -y

apt install eatmydata -y
ln -s /usr/bin/eatmydata /usr/local/bin/apt
ln -s /usr/bin/eatmydata /usr/local/bin/apt-get
ln -s /usr/bin/eatmydata /usr/local/bin/apt-dpkg

unzip artifacts.zip

mkdir /var/lib/freedombox
touch /var/lib/freedombox/is-freedombox-disk-image
DEBIAN_FRONTEND=noninteractive apt install -y ./debian/output/freedombox*_all.deb

# Fix DNS issues
systemctl enable systemd-resolved
systemctl start systemd-resolved
