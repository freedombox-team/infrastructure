###########################################
# Daily check for new freedombox packages #
###########################################

resource "aws_cloudwatch_event_rule" "freedombox_ami_build_trigger" {
  name                = "freedombox-ami-build-trigger"
  description         = "Do a daily check for new FreedomBox deb packages and trigger a build if available"
  is_enabled          = true
  schedule_expression = "rate(1 day)"
  tags = {
    "Schedule" = "daily"
  }
}

resource "aws_cloudwatch_event_target" "freedombox_ami_build_trigger_target" {
  rule = aws_cloudwatch_event_rule.freedombox_ami_build_trigger.name
  arn  = aws_lambda_function.ami_build_trigger_lambda_function.arn
}

#################################
# Delete older AMIs after build #
#################################

resource "aws_cloudwatch_event_rule" "cleanup_after_codebuild" {
  name        = "cleanup-after-codebuild"
  description = "Delete older AMIs after successful Packer build"
  is_enabled  = true
  event_pattern = jsonencode(
    {
      detail = {
        build-status = [
          "SUCCEEDED",
        ]
      }
      detail-type = [
        "CodeBuild Build State Change",
      ]
      source = [
        "aws.codebuild",
      ]
    }
  )
  tags = {
    "Type" = "Event-based Trigger"
  }
}

resource "aws_cloudwatch_event_target" "cleanup_after_codebuild_target" {
  rule = aws_cloudwatch_event_rule.cleanup_after_codebuild.name
  arn  = aws_lambda_function.cleanup_function.arn
}

##################
# Weekly cleanup #
##################

resource "aws_cloudwatch_event_rule" "cleanup_trigger" {
  name                = "cleanup-trigger"
  description         = "Trigger cleanup of leftover snapshots and AMIs"
  is_enabled          = true
  schedule_expression = "cron(0 0 ? * SUN *)"
  tags = {
    "Schedule" = "Sunday at 00:00"
  }
}

resource "aws_cloudwatch_event_target" "cleanup_trigger_target" {
  rule = aws_cloudwatch_event_rule.cleanup_trigger.name
  arn  = aws_lambda_function.cleanup_function.arn
}

#####################
# Demo server reset #
#####################

resource "aws_cloudwatch_event_rule" "demo_server_reset_trigger" {
  name                = "demo-server-reset-trigger"
  description         = "Trigger to reset the demo server for FreedomBox "
  is_enabled          = true
  schedule_expression = "cron(0,30 * * * ? *)"
  tags = {
    "Schedule" = "Every 30 minutes"
  }
}

resource "aws_cloudwatch_event_target" "demo_server_reset_trigger_target" {
  rule = aws_cloudwatch_event_rule.demo_server_reset_trigger.name
  arn  = aws_lambda_function.demo_reset_function.arn
}
