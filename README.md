# FreedomBox Infrastructure

Infrastructure as code for the FreedomBox continuous integration and build system.

### Components

- `ci`
  - Containing the CI resources for provisioning infrastructure to build new FreedomBox images for various hardware.
  - UI code for https://ci.freedombox.org


- `config`
  - Contains the BuildBot master configuration files to run the FreedomBox continuous integration and build system.


### Credentials storage

The BuildBot master expects credentials for user accounts and passwords for workers. These should be specified in JSON format on the build master's server at `/etc/buildbot/auth.json`.

  Copy the file auth.sample.json to `/etc/buildbot/auth.json` and replace the sample data with the correct data.

The same password as provided in auth.json has to be provided in the buildbot.tac file of each worker.


## Infrastructure as Code

Most of the deployed infrastructure on AWS is captured as code except where there Terraform still didn't have support for some of the newer services in AWS.

You just have to run `terraform apply` in the directory aws/terraform/infrastructure to get the entire infrastructure set up in a new AWS account (well, at least most of it).

### Tools used

- Hashicorp Terraform
- Python3 with boto3 library

### Build pipeline

> Build Trigger Lambda -> AWS CodeBuild pipeline -> Cleanup Lambda

### Build Trigger

This Lambda function does a daily check for availability of new versions of the `freedombox` package from Debian bullseye-backports repository. It then compares the version of the deb package with the version of the latest available FreedomBox AMI. If the deb package is newer, then a new build is triggered on CodeBuild.


### CodeBuild Pipeline

The CodeBuild pipeline requires a manual trigger since it cannot be configured to listen to our source code repository hosted on GitLab. This manual trigger is automatically triggered by the Lamdba function mentioned above. The pipeline runs `packer build` to build the FreedomBox AMI. Packer internally uses Ansible for installing the latest freedombox*.deb packages.

### Cleanup Lambda

This Lambda function goes over all the AWS regions to find and delete older AMIs and their associated volumes.


### Lambda triggers

AWS Event Bridge is currently being used to trigger our Lambda functions. We have the following triggers.

1. Daily trigger for the FreedomBox AMI build trigger Lambda
2. CodeBuild Pipeline's SUCCEEDED state as a trigger for the cleanup Lambda
3. A weekly trigger for cleanup Lambda that runs on Sunday at 00:00 as an additional measure
4. A trigger every 30 minutes for the Demo Reset Lambda


## Demo Server

The FreedomBox demo server deloyed at https://demo.freedombox.org is also managed by infrastructure code in this repository.

The process for creation of the demo server is manual and documented here:  
https://salsa.debian.org/freedombox-team/freedombox/snippets/277

### Demo Reset Lambda

This Lambda function resets the demo server at every 30th minute on the clock. It is triggered by a Scheduled Event from AWS Event Bridge.
